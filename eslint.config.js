import globals from 'globals'
import tseslint from 'typescript-eslint'
import { rules, ignores } from './config/eslint.js'
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended'
export default [
  ...tseslint.configs.recommended.map(config => {
    return {
      ...config,
      ignores,
      rules,
      files: ['src/**/*.{tsx,ts}'],
      ignores,
    }
  }),

  { languageOptions: { globals: globals.browser } },
  {
    ...eslintPluginPrettierRecommended,
    files: ['src/**/*.{jsx,tsx,ts}'],
    ignores,
  },
]
