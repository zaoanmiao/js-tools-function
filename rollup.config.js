import { terser } from 'rollup-plugin-terser'
import commonjs from 'rollup-plugin-commonjs'

import typescript from '@rollup/plugin-typescript'
import babel from 'rollup-plugin-babel'
import { version } from './package.json'
const banner =
  '/*!\n' +
  ` * tools v${version}\n` +
  ` * (c) 2021-${new Date().getFullYear()} zhenwei\n` +
  ' * Released under the MIT License.\n' +
  ' */'

export default {
  input: 'src/index.ts',
  output: [
    {
      file: 'dist/index.umd.js',
      format: 'umd',
      name: 'tools',
      banner,
    },
    {
      file: 'dist/index.js',
      format: 'es',
      name: 'tools',
      banner,
    },
  ],
  plugins: [
    typescript(),
    babel({
      exclude: 'node_modules/**', //排除node_modules
    }),
    terser({
      //压缩代码
      compress: {
        // drop_console: true //关闭console
      },
    }),
    commonjs(),
  ],
}
