#  js-tools-function

> 开发中使用到的函数

包括类型判断、防抖节流、本地缓存（有有效时间）、格式化时间、复制、cookie读写（子cookie读写）及生成RFC4122 u4版本的uuid等方法，并且支持ts

:tada::tada: :tada: 

### 安装

```shell
yarn add js-tools-function
npm install js-tools-function
pnpm add js-tools-function -D
```

### 导入

```javascript
import { debonce } from 'js-tools-function'
```

```html
<script src='./index.umd.js'></script>
<!-- 通过script标签引入，会引入全局变量tools -->
```

## 函数

### 函数节流

```javascript
import { throttleAtonce } from 'js-tools-function'
throttleAtonce(fn, timeout)
```

```javascript
import { throttleLatter } from 'js-tools-function'
throttleLatter(fn, timeout)
```

### 函数防抖

```javascript
import { debonce } from 'js-tools-function'
debonce(fn, timeout)
```

### sleep函数

```javascript
import { sleep } from 'js-tools-function'
sleep(1000).then(()=>{console.log('过了1000ms')})
```

### base64转png

```javascript
import { base64ToBlob } from 'js-tools-function'
base64ToBlob(base64)
```

### 复制内容

第一个参数为要复制的内容，第二参数为回调函数

```javascript
import { copy } from 'js-tools-function'
copy(str,()=>{ console.log('复制成功') })
```

## 缓存

本地可以设置缓存时间 ,过期就返回undefined

```js
import { cache } from "js-tools-function";
const { get, set, remove, clear } = cache

// 设置过期时间为10s
set('name','张三',10*1000)

// 读取
get('name')

// 移除某个值
remove('name')

// 清空缓存
clear()
```

## 字符串

### 转化为小驼峰

将字母前面有-或者\_的转化为大写字母

```js
import { string } from 'js-tools-function'
string.camelCase(string);
// eg
string.camelCase('font-size')
// fontSize
```

## Math

```js
import { math } from 'js-tools-function'
```

**四舍五入**

```js
// 取整的四舍五入
math.rounding(1.1) // 1
```


## 随机

### 获取随机数

```js
import { getUuid } from 'js-tools-function'
getUuid() // 返回符合RFC4122 u4版本的uuid
// 只能运用于本地127.0.0.1或者https
// 4aa00c56-05db-4354-86ca-2ced34a877e7
```

### 获取给定长度的随机数

```js
import { getRandom } from 'js-tools-function'
getRandom(length = 10,list)
// 默认从0~f中随机
// 第二参数为数组，可以传递范围的数组，将会从数组中随机
getRandom(5)
// 3fac1
```

## 时间

### 格式化时间

根据所传的格式日期的字段，来格式化日期，不传time默认返回当前时间

不传str默认样式为: YYYY-MM-DD hh:mm:ss

```js
import { time } from 'js-tools-function'
time.format(str,time)
// 2022-09-19 02:06:57
```

### 计算一个毫秒的时间

对应的天、时、分、秒

```js
import { getTime } from 'js-tools-function'
getTime(99999999)
// {day: 1, hours: 3, minutes: 46, seconds: 39}
```

### 计算两个日期相差的时间

第一个参数可以是时间戳，也可以是日期对象

第二个参数默认为当前时间，可以传递时间戳和日期对象

```js
import { diffTwoTimes } from 'js-tools-function'
diffTwoTimes(new Date('2022-9-1'))
// {day: 30, hours: 20, minutes: 8, seconds: 26}
```

## cookie

```js
getCookie、setCookie、removeCookie、getSubCookie、setSubCookie、removeSubCookie、setGlobalCookieOptions
```

### 获取cookie

```js
getCookie('name') //
```

### 设置cookie

第三个参数是配置，包含domain（域名）、path（路径）、expires（过期时间）、maxAge（存在时间，单位是秒s）、secure（是否只存在https中）

```js
setCookie('name','张三',options)
/**
 * @param { Date } options.expires // 过期时间
 * @param { Number } options.maxAge // cookie存在的时间，单位是s
 * @param { String } options.domain // 设置cookie的域名
 * @param { String } options.path // 设置cookie的路径
 * @param { Boolean } options.secure // 是否只允许https请求携带
 */
```

### 移除cookie

```js
removeCookie('name')
```

### 获取子cookie

```js
getSubCookie('user','name')
```

### 设置子cookie

```js
setSubCookie('user','name','张三',options)
// user :父cookie键
// name: 子cookie键
// 张三：子cookie值
// options:配置（同设置cookie一样）
```

### 移除子cookie

// 移除的时候，无法继承之前cookie的配置，所以需要重新设置

```js
removeSubCookie('user','name')
```

### 设置默认配置

```js
setGlobalOptions(options) // 设置默认配置，后续默认使用该配置，在默认配置会被后面传递的配置覆盖（覆盖传入的那一次）
```
